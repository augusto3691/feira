<?php
/**
 * Created by PhpStorm.
 * User: Isa
 * Date: 11/04/2018
 * Time: 10:39
 */

namespace Application\Model;

use Zend\Db\Adapter\ParameterContainer;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Sql\Select;

class InscricoesTable extends AbstractTableGateway
{
    protected $table = 'inscricoes';

    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    public function fetchAll()
    {
        $select = new Select($this->table);

        $statement = $this->adapter->createStatement();
        $select->prepareStatement($this->adapter, $statement);
        $resultSet = $statement->execute();
        return $resultSet->getResource()->fetchAll(\PDO::FETCH_ASSOC);

    }

}
