<?php

namespace Application\Services;

use PhpOffice\PhpSpreadsheet\IOFactory;
use Zend\Db\TableGateway\TableGateway;

/**
 * Created by PhpStorm.
 * User: Augusto
 * Date: 3/28/2018
 * Time: 10:15 PM
 */
class ConfigReader
{

    public function __construct($adapter)
    {
        $this->adapter = $adapter;
        $this->file = 'C:\xampp\htdocs\feira\public\config.xlsx';
    }

    public function getRawSheetData()
    {
        $baseData = IOFactory::load($this->file);
        $rawSheetData = $baseData->getActiveSheet()->toArray(null, false, true, true);
        return $rawSheetData;
    }

    public function getCidades()
    {
        $sheetData = $this->getRawSheetData();
        $cidades = array_unique(array_column($sheetData, 'A'));
        sort($cidades);

        //Colocando São Paulo em primeiro
        unset($cidades[array_search('São Paulo', $cidades)]);
        array_unshift($cidades, 'São Paulo');

        return array_filter($cidades);
    }

    public function getSegmentoByCidade($cidade)
    {
        $sheetData = $this->getRawSheetData();
        $cidadeData = array_filter($sheetData, function ($el) use ($cidade) {
            return $el['A'] == $cidade;
        });

        $segmentos = array_unique(array_column($cidadeData, 'B'));

        sort($segmentos);
        return array_filter($segmentos);
    }

    public function getCursosByCidadeAndSegmento($cidade, $segmento)
    {
        $sheetData = $this->getRawSheetData();
        $cidadeData = array_filter($sheetData, function ($el) use ($cidade) {
            return $el['A'] == $cidade;
        });
        $segmentoData = array_filter($cidadeData, function ($el) use ($segmento) {
            return $el['B'] == $segmento;
        });

        $cursos = array_unique(array_column($segmentoData, 'C'));

        sort($cursos);
        return array_filter($cursos);
    }

    public function getTurmasByCidadeAndSegmentoAndCurso($cidade, $segmento, $curso)
    {
        $sheetData = $this->getRawSheetData();
        $inscricaoTable = new TableGateway('inscricoes', $this->adapter);

        $cidadeData = array_filter($sheetData, function ($el) use ($cidade) {
            return $el['A'] == $cidade;
        });
        $segmentoData = array_filter($cidadeData, function ($el) use ($segmento) {
            return $el['B'] == $segmento;
        });
        $turmas = array_filter($segmentoData, function ($el) use ($curso) {
            return $el['C'] == $curso;
        });

        $result = [];

        foreach ($turmas as $keyTurma => $turma) {
            $result[$keyTurma] = $turma;
            $result[$keyTurma]['countValidate'] = count($inscricaoTable->select(['turma_id' => $turma['D']])->toArray());
        }

        usort($result, function ($a, $b) {
            if ($a["E"] == $b["E"]) {
                return 0;
            }
            return ($a["E"] < $b["E"]) ? -1 : 1;
        });

        return $result;
    }

    public function getTurmaById($turmaId)
    {
        $sheetData = $this->getRawSheetData();
        $turma = array_filter($sheetData, function ($el) use ($turmaId) {
            return $el['D'] == $turmaId;
        });

        return current(array_values($turma));
    }

}