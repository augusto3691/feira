<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Model\InscricoesTable;
use Application\Services\ConfigReader;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Zend\Db\TableGateway\TableGateway;
use Zend\Mvc\Controller\AbstractActionController;
use Application\Controller\Plugin\Messages;
use Zend\Validator\Db\RecordExists;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        $viewModel = new ViewModel();
        $adapter = $this->getServiceLocator()->get('dbAdapter');
        $configReader = new ConfigReader($adapter);
        $cidades = $configReader->getCidades();

        $viewModel->setVariable('cidades', $cidades);
        $viewModel->setVariable('fallBack', 'a cidade desejada');
        return $viewModel;
    }

    public function segmentosAction()
    {
        $viewModel = new ViewModel();
        $adapter = $this->getServiceLocator()->get('dbAdapter');
        $configReader = new ConfigReader($adapter);
        $cidade = $this->params()->fromPost('cidade');
        $segmentos = $configReader->getSegmentoByCidade($cidade);

        $viewModel->setVariable('segmentos', $segmentos);
        $viewModel->setVariable('cidade', $cidade);
        $viewModel->setVariable('fallBack', 'o segmento desejado');
        return $viewModel;
    }

    public function cursosAction()
    {
        $viewModel = new ViewModel();
        $adapter = $this->getServiceLocator()->get('dbAdapter');
        $configReader = new ConfigReader($adapter);
        $cidade = $this->params()->fromPost('cidade');
        $segmento = $this->params()->fromPost('segmento');
        $cursos = $configReader->getCursosByCidadeAndSegmento($cidade, $segmento);

        $viewModel->setVariable('cursos', $cursos);
        $viewModel->setVariable('cidade', $cidade);
        $viewModel->setVariable('segmento', $segmento);
        $viewModel->setVariable('fallBack', 'o curso desejado');
        return $viewModel;
    }

    public function turmasAction()
    {
        $viewModel = new ViewModel();
        $adapter = $this->getServiceLocator()->get('dbAdapter');
        $configReader = new ConfigReader($adapter);
        $cidade = $this->params()->fromPost('cidade');
        $segmento = $this->params()->fromPost('segmento');
        $curso = $this->params()->fromPost('curso');

        $turmas = $configReader->getTurmasByCidadeAndSegmentoAndCurso($cidade, $segmento, $curso);
        $viewModel->setVariable('turmas', $turmas);
        $viewModel->setVariable('cidade', $cidade);
        $viewModel->setVariable('segmento', $segmento);
        return $viewModel;
    }

    public function inscricaoAction()
    {
        $viewModel = new ViewModel();
        $adapter = $this->getServiceLocator()->get('dbAdapter');
        $inscricoesTable = new TableGateway('inscricoes', $adapter);
        $configReader = new ConfigReader($adapter);
        $turmaId = $this->params()->fromRoute('turmaId');
        $turma = $configReader->getTurmaById($turmaId);

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();

            $validatorCPF = new RecordExists(array(
                'table' => 'inscricoes',
                'field' => 'cpf',
                'adapter' => $adapter
            ));

            if ($validatorCPF->isValid($data['cpf'])) {
                $this->Messages()->add("Este CPF já esta cadastrado em uma turma", Messages::TYPE_SWAL_DANGER);
                return $this->redirect()->toRoute('home');
            } else {

                $data['turma_id'] = $turmaId;
                $data['turma_data_inicio'] = $turma['E'];
                $data['turma_data_fim'] = $turma['F'];
                $data['turma_hora_inicio'] = $turma['G'];
                $data['turma_hora_fim'] = $turma['H'];
                $data['turma_endereco'] = $turma['I'];
                $data['turma_local'] = $turma['J'];

                $inscricoesTable->insert($data);

                $this->Messages()->add("Inscrição efetuada com sucesso", Messages::TYPE_SWAL_SUCCESS);
                return $this->redirect()->toRoute('home');
            }
        }

        $viewModel->setVariable('turma', $turma);
        $viewModel->setVariable('isForm', true);
        return $viewModel;
    }

    public function exportAction()
    {
        $adapter = $this->getServiceLocator()->get('dbAdapter');
        $inscricoesTable = new TableGateway('inscricoes', $adapter);

        $header = [
            'Id',
            'Id da Turma',
            'Data de Inicio da Turma',
            'Data de Fim da Turma',
            'Hora de Inicio da Turma',
            'Hora de Fim da Turma',
            'Endereço da Turma',
            'Local',
            'Nome do Inscrito',
            'CPF',
            'Genero',
            'Social',
            'Nascimento',
            'Escolaridade',
            'Deficiência',
            'CEP',
            'Endereço',
            'Numero',
            'Complemento',
            'Bairro',
            'UF',
            'Cidade',
            'Telefone',
            'Celular',
            'Email',
            'Mei',
            'CNPJ Mei',
            'Razão Social Mei',
            'Data Inscrição',
        ];

        $styleHeader = [
            'font' => [
                'bold' => true,
            ],
        ];


        $data = $inscricoesTable->select()->toArray();
        usort($data, function ($a, $b) {
            if ($a["turma_id"] == $b["turma_id"]) {
                return 0;
            }
            return ($a["turma_id"] < $b["turma_id"]) ? -1 : 1;
        });
        array_unshift($data, $header);

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
            ->setCreator("Sebrae São Paulo")
            ->setLastModifiedBy("Sebrae São Paulo")
            ->setTitle("Inscrições Super Mei")
            ->setSubject("Inscrições Super Mei")
            ->setDescription(
                "Relatório de Inscrições do Super Mei"
            )
            ->setKeywords("super mei relatorio feira sebrae")
            ->setCategory("Relatórios");

        $spreadsheet->getActiveSheet()->getStyle('A1:AA1')->applyFromArray($styleHeader);

        $spreadsheet->getActiveSheet()->fromArray($data, null, 'A1');

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Inscrições Feira Super Mei.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

    }

    public function ajaxConsoleAction()
    {
        $jsonModel = new JsonModel();
        $adapter = $this->getServiceLocator()->get('dbAdapter');
        $inscricoesTable = new TableGateway('inscricoes', $adapter);

        $jsonModel->setVariable('totalCount', $inscricoesTable->select()->count());
        return $jsonModel;
    }

    public function reportAction()
    {
        $adapter = $this->getServiceLocator()->get('dbAdapter');
        $inscricoesTable = new InscricoesTable($adapter);
        $inscricoes = $inscricoesTable->fetchAll();

        $now = new \DateTime();
        foreach ($inscricoes as $key => $inscricao) {
            $inscricoes[$key]['anos'] = $now->diff(\DateTime::createFromFormat('d/m/Y', $inscricao['nascimento']))->format('%y');
            $inscricoes[$key]['created_at'] = substr($inscricao['created_at'], 0, 10);
        }
        $qtdTotalInscricoes = count($inscricoes);

        // Group By
        $turmas = $this->arrayGroupBy($inscricoes, 'turma_id');
        foreach ($turmas as $turmaId => $inscritos) {
            $turmas[$turmaId] = count($inscritos);
        }

        $idades = $this->arrayGroupBy($inscricoes, 'anos');
        foreach ($idades as $idade => $inscritos) {
            $idades[$idade] = count($inscritos);
        }

        $escolaridades = $this->arrayGroupBy($inscricoes, 'escolaridade');
        foreach ($escolaridades as $escolaridade => $inscritos) {
            $escolaridades[$escolaridade] = count($inscritos);
        }

        $deficiencias = $this->arrayGroupBy($inscricoes, 'deficiencia');
        foreach ($deficiencias as $deficiencia => $inscritos) {
            $deficiencias[$deficiencia] = count($inscritos);
        }

        $dias = $this->arrayGroupBy($inscricoes, 'created_at');
        foreach ($dias as $dia => $inscritos) {
            $dias[$dia] = count($inscritos);
        }

        //Variaveis
        $qtdHomens = count(array_filter($inscricoes, function ($i) {
            return $i['genero'] == 'Masculino';
        }));
        $qtdMulheres = $qtdTotalInscricoes - $qtdHomens;

        ////////////////////////////////////////////////////////////////////////

        $qtdMei = count(array_filter($inscricoes, function ($i) {
            return $i['mei'] == 'Sim';
        }));
        $qtdNaoMei = $qtdTotalInscricoes - $qtdMei;

        ////////////////////////////////////////////////////////////////////////

        $qtdCapital = count(array_filter($inscricoes, function ($i) {
            return $i['cidade'] == 'SÃO PAULO';
        }));
        $qtdForaCapital = $qtdTotalInscricoes - $qtdCapital;

        ////////////////////////////////////////////////////////////////////////

        $qtdTurma1 = count(array_filter($turmas, function ($i) {
            return $i == 1;
        }));
        $qtdTurma2_5 = count(array_filter($turmas, function ($i) {
            return ($i >= 2 && $i <= 5);
        }));
        $qtdTurma6_10 = count(array_filter($turmas, function ($i) {
            return ($i >= 6 && $i <= 10);
        }));
        $qtdTurma10_16 = count(array_filter($turmas, function ($i) {
            return ($i >= 10 && $i <= 16);
        }));
        $qtdTurma16more = count(array_filter($turmas, function ($i) {
            return ($i > 16);
        }));

        ////////////////////////////////////////////////////////////////////////

        $qtdIdade16_20 = array_sum(array_filter($idades, function ($k) {
            return $k >= 16 && $k <= 20;
        }, ARRAY_FILTER_USE_KEY));
        $qtdIdade21_30 = array_sum(array_filter($idades, function ($k) {
            return ($k >= 21 && $k <= 30);
        }, ARRAY_FILTER_USE_KEY));
        $qtdIdade31_40 = array_sum(array_filter($idades, function ($k) {
            return ($k >= 31 && $k <= 40);
        }, ARRAY_FILTER_USE_KEY));
        $qtdIdade41_50 = array_sum(array_filter($idades, function ($k) {
            return ($k >= 41 && $k <= 50);
        }, ARRAY_FILTER_USE_KEY));
        $qtdIdade50more = array_sum(array_filter($idades, function ($k) {
            return ($k > 50);
        }, ARRAY_FILTER_USE_KEY));

        ////////////////////////////////////////////////////////////////////////

    }


    public function arrayGroupBy(array $array, $key)
    {
        if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key)) {
            trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
            return null;
        }

        $func = (!is_string($key) && is_callable($key) ? $key : null);
        $_key = $key;

        // Load the new array, splitting by the target key
        $grouped = [];
        foreach ($array as $value) {
            $key = null;

            if (is_callable($func)) {
                $key = call_user_func($func, $value);
            } elseif (is_object($value) && isset($value->{$_key})) {
                $key = $value->{$_key};
            } elseif (isset($value[$_key])) {
                $key = $value[$_key];
            }

            if ($key === null) {
                continue;
            }

            $grouped[$key][] = $value;
        }

        // Recursively build a nested grouping if more parameters are supplied
        // Each grouped array value is grouped according to the next sequential key
        if (func_num_args() > 2) {
            $args = func_get_args();

            foreach ($grouped as $key => $value) {
                $params = array_merge([$value], array_slice($args, 2, func_num_args()));
                $grouped[$key] = call_user_func_array('arrayGroupBy', $params);
            }
        }

        return $grouped;
    }

}
