<?php

return array(
    'view_manager' => array(
        'doctype' => 'HTML5',
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
            'Messages' => 'UI\View\Helper\Messages',
            'PageTitle' => 'UI\View\Helper\PageTitle',
            'DataTable' => 'UI\View\Helper\DataTable',
        )
    ),
);
