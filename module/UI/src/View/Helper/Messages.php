<?php

namespace UI\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @ViewHelper
 * 
 * @author Augusto Coelho
 */
class Messages extends AbstractHelper implements ServiceLocatorAwareInterface
{

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function __invoke()
    {
        $messages = $this->getServiceLocator()->getServiceLocator()->get('ControllerPluginManager')->get('Messages');
        return $this->getView()->render('helper/messages', array('messages' => $messages));
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

}
