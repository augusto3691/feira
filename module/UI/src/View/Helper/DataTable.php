<?php

namespace UI\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @ViewHelper
 *
 * @author Augusto Coelho
 */
class DataTable extends AbstractHelper implements ServiceLocatorAwareInterface
{

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function __invoke($data, $columnConfig, $register, $actions, $tableId = 'listTable')
    {
        $params = array();

        $params['data'] = $data;
        $params['columnConfig'] = $columnConfig;
        $params['register'] = $register;
        $params['actions'] = $actions;
        $params['tableId'] = $tableId;

        return $this->getView()->render('helper/dataTable', $params);
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

}