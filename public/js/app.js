$(document).ready(function () {

    /*==================================================
    Full Height Content
    ==================================================*/
    function fullHeight() {
        var sectionHeader = $('.header-super-mei');
        var sectionHeaderHeight = 0;

        if (sectionHeader.size()) {
            sectionHeaderHeight = parseInt(sectionHeader.height()) + parseInt(sectionHeader.css('padding-bottom'));
        }

        var windowsHeight = $(window).height();
        var centerConentHeight = $('.center-content').height();

        $('.center-content').css('margin-top',
            (windowsHeight / 2) - sectionHeaderHeight - centerConentHeight - 20
        );
    }

    fullHeight();

    /*==================================================
    Select Change
    ==================================================*/
    if (document.querySelector('.cs-select')) {
        new SelectFx(document.querySelector('.cs-select'), {
            onChange: function (opt) {
                $("#flow").submit();
                $(".hello").append('<p><i style="font-size: 50px; color: #95c21f;" class="fa fa-spin fa-sync-alt"></i></p>')
            }
        });
    }

    /*==================================================
    MEI Interaction
    ==================================================*/
    $('input[name="mei"]').change(function () {
        if ($(this).val() == "Sim") {
            $('#mei-info').removeClass('hidden');
        } else {
            $('#mei-info').addClass('hidden');
        }
    });

    /*==================================================
    Masks
    ==================================================*/
    $('input[name="cpf"]').mask('000.000.000-00');
    var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function (val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };
    $('input[name="celular"]').mask(SPMaskBehavior, spOptions);
    $('input[name="telefone"]').mask(SPMaskBehavior, spOptions);
    $('input[name="nascimento"]').mask("00r00r0000", {
        translation: {
            'r': {
                pattern: /[\/]/,
                fallback: '/'
            },
            placeholder: "__/__/____"
        }
    });
    $('input[name="cep"]').mask('00000-000');
    $('input[name="cnpj"]').mask('00.000.000/0000-00');

    /*==================================================
    Button block after submit
    ==================================================*/
    $('#inscricao').submit(function () {
        var btn = $('#btn-submit');
        btn.attr('disabled', 'disabled');
        btn.html('<i class="fa fa-spin fa-sync-alt"></i>')
    })

    /*==================================================
    AutoComplete Adress
    ==================================================*/
    if ($('input[name="cep"]').length) {
        $('input[name="cep"]').autocompleteAddress();
    }

    /*==================================================
    HotKeys
    ==================================================*/
    if ($('#table-console tbody').length) {
        $(document).bind('keydown', 'ctrl+k', function (event) {
            var $tBody = $('#table-console tbody');
            event.preventDefault();
            $.ajax({
                url: "/ajax-console",
                method: "GET",
                success: function (data) {
                    $tBody.html('');
                    $tBody.append(
                        '<tr>' +
                        '<td>Numero de Inscrições Total</td>' +
                        '<td><strong>' + data.totalCount + '</strong></td>' +
                        '</tr>')
                },
                error: function (error) {
                    console.log(error);
                }
            });

            $('#consoleModal').modal('show');
            return false;
        });
    }

    /*==================================================
    Virtual Keyboard
    ==================================================*/
    if ($('input[type="text"]').length) {
        $('input[type="text"]').accentKeyboard({
            active_shift: false,
            layout: 'accent',
            open_speed: 1,
            close_speed: 1,
        });
        $('input[type="text"]').on('change', function () {
            $(this).trigger('input');
        });
    }

    /*==================================================
    Scroll To
    ==================================================*/
    $('input[type="text"]').focus(function () {
        $('html, body').animate({
            scrollTop: $(this).offset().top - 35
        }, 200);
    });


});